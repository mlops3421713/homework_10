# MLops homework: k8s

This is a homework repository for the [MLOps & production for data science research 3.0](https://ods.ai/tracks/mlops3-course-spring-2024).

## Setting up the service using kubectl:

`kubectl apply -f nginx-html.yaml deployment.yaml -f service.yaml`

The app is configured as NodePort. You can use `minicube service list` or similar command for your environment to find out.

Testing the localhost  exposure (not recommended but requires no load balancer or ingress contoller):

`kubectl port-forward service/nginx <your desired port>:80`

Live demo is up at https://pool.animeco.in/k8s-demo/

# Disclaimer as of Aug 16th 2024:
After running the live demo for two months, I've brought it down. If you need it yet, ping me in whatever way you prefer (e.g. tg://@daiyousei) and I'll do `minikube start` for your royal pleasure.